<header id="page-header">
    <!-- Header Content -->
    <div class="content-header">
        <!-- Left Section -->
        <div class="d-flex align-items-center">
            <!-- Logo -->
            <a class="fw-semibold fs-5 tracking-wider text-dual me-3" href="index.html"> EMPLOYEES SALARY BREAKDOWN INFORMATION SYSTEM </span>
            </a>
            <!-- END Logo -->
        </div>
        <!-- END Left Section -->
        <!-- Right Section -->
        <div class="d-flex align-items-center">
            <!-- User Dropdown -->
            <div class="d-inline-block ms-2">
                <button type="button" class="btn btn-alt-secondary">
                    <img class="rounded-circle" src="{{ url('assets/media/avatars/avatar10.jpg') }}" alt="Header Avatar" style="width: 21px;">
                    <span class="d-none d-sm-inline-block ms-1">Aap Saepulloh, S.Tr.Akun., M.M.</span>
                </button>
            </div>
            <!-- END User Dropdown -->
        </div>
        <!-- END Right Section -->
    </div>
    <!-- END Header Content -->
</header>