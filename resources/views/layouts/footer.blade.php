<footer id="page-footer" class="bg-primary-darker">
  <div class="content py-3">
    <div class="row fs-sm">
      <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-end">
        Made possible by <a class="fw-semibold" href="https://www.linkedin.com/in/aldyfrediant/" target="_blank">Aldy Frediant</a>
      </div>
      <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-start">
        <a class="fw-semibold" href="" target="_blank">ESBIS Version | v1.11.01-hotfix</a> © <span data-toggle="year-copy" class="js-year-copy-enabled">{{ date('Y') }}</span>
      </div>
    </div>
  </div>
</footer>