<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/vendors/css/charts/apexcharts.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/vendors/css/extensions/toastr.min.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/components.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/themes/dark-layout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/themes/bordered-layout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/themes/semi-dark-layout.css') }}">

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/pages/dashboard-ecommerce.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/plugins/charts/chart-apex.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/app/css/plugins/extensions/ext-component-toastr.css') }}">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ url('/assets/css/style.css') }}">
<!-- END: Custom CSS-->