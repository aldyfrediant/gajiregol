<!-- BEGIN: Vendor JS-->
<script src="{{ url('/assets/app/vendors/js/vendors.min.js') }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ url('/assets/app/js/core/app-menu.js') }}"></script>
<script src="{{ url('/assets/app/js/core/app.js') }}"></script>
<!-- END: Theme JS-->
<script>
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    })
</script>