<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title>ESBIS | Kecamatan Regol</title>

  <meta name="description" content="OneUI - Bootstrap 5 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
  <meta name="author" content="pixelcave">
  <meta name="robots" content="noindex, nofollow">

  <!-- Icons -->
  <link rel="shortcut icon" href="{{ url('assets/media/favicons/little-monster.png') }}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{ url('assets/media/favicons/little-monster.png') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ url('assets/media/favicons/little-monster.png') }}">
  <!-- END Icons -->
  <link rel="stylesheet" id="css-main" href="{{ url('assets/css/oneui.min.css') }}">

  @yield('addCSS')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->


<div id="page-container" class="page-header-dark main-content-boxed">
  @include('layouts.header')

  <main id="main-container">
    @include('layouts.nav')

    <!-- Page Content -->
    <div class="content">
      @yield('content')
    </div>
    <!-- END Page Content -->
  </main>

  @include('layouts.footer')
  <script src="{{ url('assets/js/oneui.app.min.js') }}"></script>
  <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
  @yield('addJS')
  </body>
  <!-- END: Body-->

</html>