<!-- Navigation -->
<div class="bg-primary-darker">
    <div class="content py-3">
        <!-- Toggle Main Navigation -->
        <div class="d-lg-none">
            <!-- Class Toggle, functionality initialized in Helpers.oneToggleClass() -->
            <button type="button" class="btn w-100 btn-alt-secondary d-flex justify-content-between align-items-center" data-toggle="class-toggle" data-target="#main-navigation" data-class="d-none"> Menu <i class="fa fa-bars"></i>
            </button>
        </div>
        <!-- END Toggle Main Navigation -->
        <!-- Main Navigation -->
        <div id="main-navigation" class="d-none d-lg-block mt-2 mt-lg-0">
            <ul class="nav-main nav-main-dark nav-main-horizontal nav-main-hover">
                <li class="nav-main-item">
                    <a class="nav-main-link active" href="{{ url('karyawan') }}">
                        <i class="nav-main-link-icon si si-speedometer"></i>
                        <span class="nav-main-link-name"></span>
                    </a>
                </li>
                <div class="d-inline-block ms-2">
                    <button type="button" class="btn btn-outline-danger" style="width: 260px">
                        <i class="nav-main-link-icon fa fa-fw fa-bank"></i>
                        <span class="nav-main-link-name">Dashboard</span>
                    </button>
                </div>
                <div class="d-inline-block ms-2">
                    <button type="button" class="btn btn-outline-success" style="width: 260px">
                        <i class="nav-main-link-icon si si-people"></i>
                        <span class="nav-main-link-name">Data Aparatur</span>
                    </button>
                </div>
                <div class="d-inline-block ms-2">
                    <button type="button" class="btn btn-outline-warning" style="width: 260px">
                        <i class="nav-main-link-icon fa fa-fw fa-book"></i>
                        <span class="nav-main-link-name">Data Penghasilan</span>
                    </button>
                </div>
                <div class="d-inline-block ms-2">
                    <button type="button" class="btn btn-outline-info" style="width: 260px">
                        <i class="nav-main-link-icon si si-briefcase"></i>
                        <span class="nav-main-link-name">Data Slip Bayar</span>
                    </button>
                </div>
                <li class="nav-main-item ms-2">
                    <a class="nav-main-link active">
                        <i class="nav-main-link-icon si si-speedometer"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- END Main Navigation -->
    </div>
</div>
<!-- END Navigation -->