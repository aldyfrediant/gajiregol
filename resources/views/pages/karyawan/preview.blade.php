@extends('layouts.app')

@section('addCSS')
<style>
    .btn-block {
        display: block;
        width: 100%;
    }

    .--slip-gaji-preview {
        font-family: serif;
        color: black;
        padding: 12px;
    }

    .--kop {
        padding: 16px 0;
        text-align: center;
    }

    .--kop table {
        margin: auto;
        vertical-align: center;
        border-bottom: 4px solid black;
        padding: 12px 0;
    }

    .--kop h3 {
        font-size: 24.5px;
        margin: 0;
    }

    .--kop h1 {
        margin: 0;
    }

    .--kop p {
        margin: 0;
        font-size: 14px;
    }

    .--header {
        margin: 0;
    }

    .--header h5 {
        margin: 0;
    }

    .--header p {
        margin: 0;
    }

    .--biodata {
        padding: 12px 5%;
        font-size: 14px;
    }

    .--biodata p {
        margin: 0;
    }

    .--penghasilan table,
    .--potongan table {
        border-collapse: collapse;
    }

    .--penghasilan table th,
    .--potongan table th {
        border: 1px solid black;
        background-color: lightgray;
        font-weight: bold;
        padding: 2px 6px;
    }

    .--penghasilan td,
    .--potongan td {
        font-size: 14px;
        border: 1px solid black;
        padding: 2px 4px;
    }

    .--penghasilan td:first-child,
    .--potongan td:first-child {
        text-align: center;
    }

    .--penghasilan,
    .--potongan,
    .--footer {
        padding: 12px 5%;
    }

    .denomination {
        display: inline-block;
        margin: 0;
        width: 10%;
    }

    .--penghasilan td span:not(.denomination),
    .--potongan td span:not(.denomination) {
        text-align: right;
        display: inline-block;
        width: 85%;
    }

    p {
        margin: 0;
    }

    .--footer p {
        margin: 0;
    }

    .--ttd {
        width: 35%;
        margin: 16px 0;
        margin-left: auto;
        text-align: right;
    }

    .--note p {
        font-size: 12px;
        text-align: center;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-8">
        <div class="--slip-gaji-preview block">
            <div class="--kop">
                <table width="90%">
                    <tr>
                        <td width="100px">
                            <img src="{{ url('assets/img/kota-bdg.png') }}" width="100px" alt="">
                        </td>
                        <td>
                            <h3>PEMERINTAH KOTA BANDUNG</h3>
                            <h1>KECAMATAN REGOL</h1>
                            <p>Jl. Denki No. 54 Bandung 40255 Telp. (022) 5202489</p>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="--header">
                <div class="text-center">
                    <h5 style="text-decoration: underline;">SURAT KETERANGAN PERINCIAN PENGHASILAN</h5>
                    <p>Nomor : KU.07/[No.Reg]-Kec.Rgl/SKPG/[Bulan]/[Tahun]</p>
                </div>
            </div>

            <div class="--biodata">

                <p align="justify">
                    Yang bertandatangan dibawah ini, Bendahara Pengeluaran / Pengelola Gaji Aparatur Sipil Negara pada Pemerintah Kota
                    Bandung Kecamatan Regol dengan ini menyatakan bahwa :
                </p>
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td width="50%">Nama</td>
                                    <td>: {{ $k->nama_lengkap }}</td>
                                </tr>
                                <tr>
                                    <td>NIP</td>
                                    <td>: {{ $k->nip }}</td>
                                </tr>
                                <tr>
                                    <td>Tempat / Tgl. Lahir</td>
                                    <td>: {{ $k->tempat_lahir.', '.date('d M Y', strtotime($k->tanggal_lahir)) }}</td>
                                </tr>
                                <tr>
                                    <td>Pangkat / Gol. Ruang</td>
                                    <td>: {{ $k->pangkat.' - '.$k->golongan }}</td>
                                </tr>
                                <tr>
                                    <td>Masa Kerja Golongan</td>
                                    @php
                                    $start = $k->mulai_kerja;
                                    $today = date("Y-m-d");
                                    $diff = date_diff(date_create($start), date_create($today));
                                    @endphp
                                    <td>: {{ $diff->format('%Y'). ' Tahun '.$diff->format('%M'). ' Bulan' }}</td>
                                </tr>
                            </table>
                        </td>
                        <td style="display: flex; align-items: start;">
                            <table>
                                <tr>
                                    <td>Status Perkawinan</td>
                                    @if($k->status_kawin == 'k')
                                    <td>
                                        : <del>Belum Kawin</del> / Kawin
                                    </td>
                                    @else
                                    <td>
                                        : Belum Kawin / <del>Kawin</del>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Status Pegawai</td>
                                    @if($k->status_pegawai == 'asn')
                                    <td>: Aparatur Sipil Negara - PNS</td>
                                    @else
                                    <td>: Aparatur Sipil Negara - PPPK</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Unit Kerja</td>
                                    <td>: {{ $k->unit_kerja }}</td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td>: {{ $k->jabatan }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="--penghasilan">
                <table width="100%">
                    <thead>
                        <tr>
                            <th colspan=4>Perincian Penghasilan Bulan : [Bulan] [Tahun]</th>
                        </tr>
                    </thead>
                    <tr>
                        <td width="5%">1.</td>
                        <td width="45%">Gaji Pokok</td>
                        <td width="25%"><span class="denomination">Rp</span> <span>{{ number_format($g->gaji_pokok) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Tunjangan Suami / Istri</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_pasangan) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Tunjangan Anak</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_anak) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Tunjangan Struktural</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_struktural) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>Tunjangan Umum</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_umum) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Tunjangan Fungsional</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_fungsional) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td>Tunjangan Khusus</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_khusus) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>8.</td>
                        <td>Tunjangan Beras</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_beras) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>Tunjangan Pajak PPh Ps. 21</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tunjangan_pph) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>Pembulatan</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->pembulatan) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right; padding-right: 16px;">
                            <b>Total Gaji</b>
                        </td>
                        <td>
                            <b>
                                <span class="denomination">Rp</span> <span>{{ number_format($g->total_one) }}</span>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>11.</td>
                        <td width="135px">TPP-ASN</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tpp_asn) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right; padding-right: 16px;">
                            <b>Sub Total Penghasilan (1-11)</b>
                        </td>
                        <td>
                            <b>
                                <span class="denomination">Rp</span> <span>{{ number_format($g->subtotal_one) }}</span>
                            </b>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="--potongan">

                <table width="100%">
                    <thead>
                        <tr>
                            <th colspan=4>Pemotongan - Pemotongan</th>
                        </tr>
                    </thead>
                    <tr>
                        <td width="5%">12.</td>
                        <td width="45%">Pemotongan Pajak PPh Ps. 21</td>
                        <td width="25%"><span class="denomination">Rp</span> <span>{{ number_format($g->pemotongan_pph) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>13.</td>
                        <td>Pemotongan Iuran Wajib Pegawai 1%</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->iuran_wajib_one) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td>Pemotongan Iuran Wajib Pegawai 8%</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->iuran_wajib_eight) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td>Pemotongan Tabungan Perumahan</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->tabungan_perumahan) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>16.</td>
                        <td>Pemotongan Hutang Bank BJB</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->hutang_bjb) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>17.</td>
                        <td>Pemotongan Hutang KPKB</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->hutang_kpkb) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>18.</td>
                        <td>Pemotongan Hutang BPR</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->hutang_bpr) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>19.</td>
                        <td>Pemotongan Zakat Profesi</td>
                        <td><span class="denomination">Rp</span> <span>{{ number_format($g->zakat) }}</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right; padding-right: 16px;">
                            <b>Sub Total Pemotongan (12-19)</b>
                        </td>
                        <td>
                            <b>
                                <span class="denomination">Rp</span> <span>{{ number_format($g->subtotal_two) }}</span>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <h4 style="margin: 0;text-align: right; padding-right: 16px;">Jumlah yang diterima</h4>
                        </td>
                        <td>
                            <h4 style="margin: 0;">
                                <span class="denomination">Rp</span> <span>{{ number_format($g->grandtotal) }}</span>
                            </h4>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="--footer">
                <p>Demikianlah Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</p>
                <div class="--ttd">
                    <div>
                        <p>Bandung, [Auto]</p>
                        <b>Pengelola Gaji</b>
                        <br>
                        <br>
                        <br>
                        <br>
                        <b><u>Rudi Rustandi</u></b>
                        <p>NIP. 19670622 200701 1 013</p>
                    </div>
                </div>
                <div class="--note">
                    <p>
                        <i>Motto Juang Kecamatan Regol : Bangkit Bersama Membangun Regol Lebih Baik</i>
                    </p>
                </div>
            </div>
        </div>

    </div>
    <div class="col-4">
        <div class="block">
            <div class="block-header bg-secondary">
                <h3 class="block-title text-white">Print Slip Gaji</h3>
            </div>
            <div class="block-content block-content-full">
                {!! Form::open(['method' => 'POST', 'route' => ['history.store', $k->id]]) !!}
                <div class="row">
                    <div class="form-group mb-3">
                        {!! Form::label('', 'Bulan', ['class' => 'form-label']) !!}
                        {!! Form::select('bulan', [
                        '01' => 'Januari',
                        '02' => 'Februari',
                        '03' => 'Maret',
                        '04' => 'April',
                        '05' => 'Mei',
                        '06' => 'Juni',
                        '07' => 'Juli',
                        '08' => 'Agustus',
                        '09' => 'September',
                        '10' => 'Oktober',
                        '11' => 'Nopember',
                        '12' => 'Desember'
                        ], null, ['class' => 'form-control', 'placeholder' => 'Pilih bulan']) !!}
                    </div>
                    <div class="form-group mb-3">
                        {!! Form::label('', 'Tahun', ['class' => 'form-label']) !!}
                        {!! Form::select('tahun', [
                        '2019' => '2019',
                        '2020' => '2020',
                        '2021' => '2021',
                        '2022' => '2022',
                        '2023' => '2023',
                        '2024' => '2024',
                        '2025' => '2025',
                        '2026' => '2026',
                        '2027' => '2027',
                        '2028' => '2028',
                        '2029' => '2029',
                        '2030' => '2030',
                        '2031' => '2031'
                        ], null, ['class' => 'form-control', 'placeholder' => 'Pilih tahun']) !!}
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-success my-3">
                    <i class="fas fa-print"></i>
                    Print Data
                </button>
                <a href="{{ route('karyawan.show', $k->id)}}" class="btn btn-block btn-warning">
                    <i class="fas fa-edit"></i>
                    Edit Data
                </a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('addJS')
@endsection