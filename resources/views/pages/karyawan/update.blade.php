@extends('layouts.app')

@section('addCSS')
<link rel="stylesheet" href="{{ url('assets/js/plugins/flatpickr/flatpickr.min.css') }}">
<style>
    .form-gaji .form-group {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        align-items: center;
        padding: 4px 8px;
    }

    .text-right {
        text-align: right;
    }
</style>
@endsection

@section('content')
<div class="row">
    {{-- Slip Gaji --}}
    <div class="col-12">
        <div class="block block-rounded">
            <div class="block-header bg-secondary">
                <div class="block-title text-white">
                    Slip Gaji Terdahulu
                </div>
                <div class="block-options">
                    <button type="submit" class="btn btn-block btn-info">
                        <i class="fas fa-print"></i> Print Slip Gaji
                    </button>
                </div>
            </div>
            <div class="block-content block-content-full">
                @forelse($history as $h)
                @php
                switch ($h->month_code) {
                case 1:
                $month = 'Januari';
                break;
                case 2:
                $month = 'Pebruari';
                break;
                case 3:
                $month = 'Maret';
                break;
                case 4:
                $month = 'April';
                break;
                case 5:
                $month = 'Mei';
                break;
                case 6:
                $month = 'Juni';
                break;
                case 7:
                $month = 'Juli';
                break;
                case 8:
                $month = 'Agustus';
                break;
                case 9:
                $month = 'September';
                break;
                case 10:
                $month = 'Oktober';
                break;
                case 11:
                $month = 'Nopember';
                break;
                case 12:
                $month = 'Desember';
                break;
                }
                @endphp
                <a href="{{ route('history.show', $h->id) }}" class="btn btn-block btn-warning mb-2">
                    <i class="fas fa-print"></i>
                    {{ $month }} {{ $h->year_code }}
                </a>
                @empty
                <div class="alert alert-danger d-flex align-items-center justify-content-between" role="alert">
                    <div class="flex-grow-1 me-3">
                        <p class="mb-0 text-center">
                            Karyawan ini belum pernah mencetak slip gaji.
                        </p>
                    </div>
                </div>
                @endforelse
            </div>
        </div>
    </div>

    {{-- Data Karyawan --}}
    <div class="col-12">
        {!! Form::model($k->id, ['method' => 'PUT', 'route' => ['karyawan.update', $k->id]]) !!}
        <div class="block block-rounded">
            <div class="block-header bg-secondary">
                <h3 class="block-title text-white">Edit Data Aparatur Sipil Negara</h3>
                <div class="block-options">
                    <button type="submit" class="btn btn-block btn-success">
                        <i class="fas fa-save"></i> Update Data
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="block-content block-content-full">
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Nama', ['class' => 'form-label']) !!}
                            {!! Form::text('nama', $k->nama_lengkap, ['required', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'NIP', ['class' => 'form-label']) !!}
                            {!! Form::text('nip', $k->nip, ['required', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Tempat Lahir', ['class' => 'form-label']) !!}
                            {!! Form::text('tempat_lahir', $k->tempat_lahir, ['required', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Tanggal Lahir', ['class' => 'form-label']) !!}

                            {!! Form::text('tanggal_lahir', date("d M Y", strtotime($k->tanggal_lahir)), ['required', 'class' => 'form-control flatpickr flatpickr-human-friendly', 'placeholder' => 'Silahkan pilih tanggal']) !!}
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Pangkat / Golongan Ruang', ['class' => 'form-label']) !!}
                            <select required name="pangkat" class="form-control selectPangkat">
                                <option disabled readonly hidden selected>Pilih pangkat/golongan</option>
                                {{-- Golongan IV --}}
                                <optgroup label="Golongan IV">
                                    <option @selected($k->pangkat == "Pembina Utama") value="Pembina Utama">Pembina Utama / IV.e</option>
                                    <option @selected($k->pangkat == "Pembina Utama Muda") value="Pembina Utama Muda">Pembina Utama Muda / IV.c</option>
                                    <option @selected($k->pangkat == "Pembina Tingkat I") value="Pembina Tingkat I">Pembina Tingkat I / IV.b</option>
                                    <option @selected($k->pangkat == "Pembina") value="Pembina">Pembina / IV.a</option>
                                </optgroup>

                                {{-- Golongan III --}}
                                <optgroup label="Golongan III">
                                    <option @selected($k->pangkat == "Penata Tingkat I") value="Penata Tingkat I">Penata Tingkat I / III.d</option>
                                    <option @selected($k->pangkat == "Penata") value="Penata">Penata / III.c</option>
                                    <option @selected($k->pangkat == "Penata Muda Tingkat I") value="Penata Muda Tingkat I">Penata Muda Tingkat I / III.b</option>
                                    <option @selected($k->pangkat == "Penata Muda") value="Penata Muda">Penata Muda / III.a</option>
                                </optgroup>

                                {{-- Golongan II --}}
                                <optgroup label="Golongan II">
                                    <option @selected($k->pangkat == "Pengatur Tingkat I") value="Pengatur Tingkat I">Pengatur Tingkat I / II.d</option>
                                    <option @selected($k->pangkat == "Pengatur") value="Pengatur">Pengatur / II.c</option>
                                    <option @selected($k->pangkat == "Pengatur Muda Tingkat I") value="Pengatur Muda Tingkat I">Pengatur Muda Tingkat I / II.b</option>
                                    <option @selected($k->pangkat == "Pengatur Muda") value="Pengatur Muda">Pengatur Muda / II.a</option>
                                </optgroup>


                                {{-- Golongan II --}}
                                <optgroup label="Golongan I">
                                    <option @selected($k->pangkat == "Juru Tingkat I") value="Juru Tingkat I">Juru Tingkat I / I.d</option>
                                    <option @selected($k->pangkat == "Juru") value="Juru">Juru / I.c</option>
                                    <option @selected($k->pangkat == "Juru Muda Tingkat I") value="Juru Muda Tingkat I">Juru Muda Tingkat I / I.b</option>
                                    <option @selected($k->pangkat == "Juru Muda") value="Juru Muda">Juru Muda / I.a</option>
                                </optgroup>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="block-content block-content-full">
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Masa Kerja Golongan', ['class' => 'form-label']) !!}
                            {!! Form::text('mulai_kerja', date("d M Y", strtotime($k->mulai_kerja)), ['required', 'class' => 'form-control flatpickr flatpickr-human-friendly', 'placeholder' => 'Silahkan pilih tanggal']) !!}
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Status Perkawinan', ['class' => 'form-label']) !!}
                            <select required name="status_kawin" class="form-control">
                                <option disabled readonly hidden selected>Pilih status kawin</option>
                                <option @selected($k->status_kawin == "bk") value="bk">Belum Kawin</option>
                                <option @selected($k->status_kawin == "k") value="k">Kawin</option>
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Status Pegawai', ['class' => 'form-label']) !!}
                            <select required name="status_pegawai" class="form-control">
                                <option disabled readonly hidden selected>Pilih status pegawai</option>
                                <option @selected($k->status_pegawai == "asn") value="asn">Aparatur Sipil Negara - PNS</option>
                                <option @selected($k->status_pegawai == "non") value="non">Aparatur Sipil Negara - PPPK</option>
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Unit Kerja', ['class' => 'form-label']) !!}
                            {!! Form::text('unit_kerja', $k->unit_kerja, ['required', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group mb-3">
                            {!! Form::label('', 'Jabatan', ['class' => 'form-label']) !!}
                            {!! Form::text('jabatan', $k->jabatan, ['required', 'class' => 'form-control']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Gaji --}}
    <div class="col-12">
        @if($route == 'create')
        {!! Form::open(['method' => 'POST', 'route' => 'gaji.store', 'class' => 'form-gaji']) !!}
        @else
        {!! Form::model($g, ['method' => 'PUT', 'route' => ['gaji.update', $g->id], 'class' => 'form-gaji']) !!}
        @endif
        <div class="block block-rounded">
            <div class="block-header bg-secondary">
                <h3 class="block-title text-white">Perincian Penghasilan</h3>
                <div class="block-options">
                    <button type="submit" class="btn btn-block btn-success">
                        <i class="fas fa-save"></i> Update Gaji
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="block-content block-content-full">
                        {!! Form::hidden('id_karyawan', $k->id) !!}
                        <div class="form-group">
                            {!! Form::label('', 'Gaji Pokok', ['class' => 'form-label']) !!}
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('gaji_pokok', number_format($g->gaji_pokok) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'gaji_pokok']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Suami / Istri', ['class' => 'form-label']) !!}
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_pasangan', number_format($g->tunjangan_pasangan) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_pasangan']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Anak', ['class' => 'form-label']) !!}
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_anak', number_format($g->tunjangan_anak) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_anak']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Struktural', ['class' => 'form-label']) !!}
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_struktural', number_format($g->tunjangan_struktural) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_struktural']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Umum', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_umum', number_format($g->tunjangan_umum) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_umum']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Fungsional', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_fungsional', number_format($g->tunjangan_fungsional) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_fungsional']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Khusus', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_khusus', number_format($g->tunjangan_khusus) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_khusus']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Beras', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_beras', number_format($g->tunjangan_beras) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_beras']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Tunjangan Pajak PPh Ps. 21', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tunjangan_pph', number_format($g->tunjangan_pph) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tunjangan_pph']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pembulatan', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('pembulatan', number_format($g->pembulatan) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'pembulatan']) !!}
                            </div>
                        </div>
                        <div class="form-group bg-info-light">
                            {!! Form::label('', 'Total Gaji', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('total_one', number_format($g->total_one) , ['class' => 'form-control mask-number text-right form-control-alt', 'readonly', 'id' => 'total_one']) !!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            {!! Form::label('', 'TPP-ASN', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tpp_asn', number_format($g->tpp_asn) , ['class' => 'form-control mask-number text-right penambahan', 'id' => 'tpp_asn']) !!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group bg-warning-light">
                            {!! Form::label('', 'Sub Total Penghasilan', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('subtotal_one', number_format($g->subtotal_one) , ['class' => 'form-control mask-number text-right form-control-alt', 'readonly', 'id' => 'subtotal_one']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="block-content block-content-full">
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan Pajak PPh Ps. 21', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('pemotongan_pph', number_format($g->pemotongan_pph) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'pemotongan_pph']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan IWP 1%', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('iuran_wajib_one', number_format($g->iuran_wajib_one) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'iuran_wajib_one']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan IWP 8%', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('iuran_wajib_eight', number_format($g->iuran_wajib_eight) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'iuran_wajib_eight']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan Taperum', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('tabungan_perumahan', number_format($g->tabungan_perumahan) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'tabungan_perumahan']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan Hutang Bank BJB', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('hutang_bjb', number_format($g->hutang_bjb) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'hutang_bjb']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan Hutang KPKB', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('hutang_kpkb', number_format($g->hutang_kpkb) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'hutang_kpkb']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan Hutang BPR', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('hutang_bpr', number_format($g->hutang_bpr) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'hutang_bpr']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('', 'Pemotongan Zakat Profesi', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('zakat', number_format($g->zakat) , ['class' => 'form-control mask-number text-right pemotongan', 'id' => 'zakat']) !!}
                            </div>
                        </div>
                        <div class="form-group bg-warning-light">
                            {!! Form::label('', 'Sub Total Pemotongan', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('subtotal_two', number_format($g->subtotal_two) , ['class' => 'form-control mask-number text-right form-control-alt', 'readonly', 'id' => 'subtotal_two']) !!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group bg-success-light">
                            {!! Form::label('', 'Grand Total', ['class' => 'form-label']) !!}

                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                {!! Form::text('grandtotal', number_format($g->grandtotal) , ['class' => 'form-control mask-number text-right form-control-alt', 'readonly', 'id' => 'grandTotal']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('addJS')
<script src="{{ url('assets/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ url('/assets/masknumber.js') }}"></script>
<script>
    // Thousand Separator
    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    $(".flatpickr").flatpickr({
        dateFormat: "d M Y"
    });
    // Field Thousand Separator
    $('.mask-number').maskNumber({
        integer: true
    });

    let gajiPokok = 0;
    let tnPasangan = 0;
    let tnAnak = 0;
    let tnStruktur = 0;
    let tnUmum = 0;
    let tnFungsi = 0;
    let tnKhusus = 0;
    let tnBeras = 0;
    let tnPPH = 0;
    let pembulatan = 0;
    let totalOne = 0;
    let tppASN = 0;
    let subOne = 0;
    let potPPH = 0;
    let iwOne = 0;
    let iwEight = 0;
    let tbRumah = 0;
    let bjb = 0;
    let kpkb = 0;
    let bpr = 0;
    let zakat = 0;
    let subTwo = 0;
    let grandTotal = 0;

    // Auto Value
    const declare = () => {
        gajiPokok = parseFloat($('#gaji_pokok').val().replace(/,/g, ''));
        tnPasangan = parseFloat($('#tunjangan_pasangan').val().replace(/,/g, ''));
        tnAnak = parseFloat($('#tunjangan_anak').val().replace(/,/g, ''));
        tnStruktur = parseFloat($('#tunjangan_struktural').val().replace(/,/g, ''));
        tnUmum = parseFloat($('#tunjangan_umum').val().replace(/,/g, ''));
        tnFungsi = parseFloat($('#tunjangan_fungsional').val().replace(/,/g, ''));
        tnKhusus = parseFloat($('#tunjangan_khusus').val().replace(/,/g, ''));
        tnBeras = parseFloat($('#tunjangan_beras').val().replace(/,/g, ''));
        tnPPH = parseFloat($('#tunjangan_pph').val().replace(/,/g, ''));
        pembulatan = parseFloat($('#pembulatan').val().replace(/,/g, ''));
        tppASN = parseFloat($('#tpp_asn').val().replace(/,/g, ''));
        potPPH = parseFloat($('#pemotongan_pph').val().replace(/,/g, ''));
        tbRumah = parseFloat($('#tabungan_perumahan').val().replace(/,/g, ''));
        iwOne = parseFloat($('#iuran_wajib_one').val().replace(/,/g, ''));
        iwEight = parseFloat($('#iuran_wajib_eight').val().replace(/,/g, ''));
        bjb = parseFloat($('#hutang_bjb').val().replace(/,/g, ''));
        kpkb = parseFloat($('#hutang_kpkb').val().replace(/,/g, ''));
        bpr = parseFloat($('#hutang_bpr').val().replace(/,/g, ''));
        zakat = parseFloat($('#zakat').val().replace(/,/g, ''));
    }

    $('.penambahan').on('change', () => {
        declare();

        totalOne = gajiPokok + tnPasangan + tnAnak + tnStruktur + tnUmum + tnFungsi + tnKhusus + tnBeras + tnPPH + pembulatan;

        subOne = totalOne + tppASN;

        zakat = subOne * 0.025;
        grandTotal = subOne - subTwo;

        $('#zakat').val(addCommas(zakat.toFixed(0)));
        $('#total_one').val(addCommas(totalOne));
        $('#subtotal_one').val(addCommas(subOne));
        $('#grandTotal').val(addCommas(grandTotal));
    });

    $('.pemotongan').on('change', () => {
        declare();

        subTwo = potPPH + iwOne + iwEight + tbRumah + bjb + kpkb + bpr + zakat;

        grandTotal = subOne - subTwo;

        $('#subtotal_two').val(addCommas(subTwo));
        $('#grandTotal').val(addCommas(grandTotal));
    });
</script>
@endsection