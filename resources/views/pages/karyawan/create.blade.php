@extends('layouts.app')

@section('addCSS')
<link rel="stylesheet" href="{{ url('assets/js/plugins/flatpickr/flatpickr.min.css') }}">
@endsection

@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default bg-secondary">
        <h3 class="block-title text-white">Tambah Aparatur Sipil Negara</h3>
    </div>
    <div class="row">
        <div class="col-6 offset-3">
            <div class="block-content block-content-full">
                {!! Form::open([
                'method' => 'POST',
                'route' => 'karyawan.store'
                ]) !!}
                <div class="form-group mb-3">
                    {!! Form::label('', 'Nama', ['class' => 'form-label']) !!}
                    {!! Form::text('nama', null, ['required', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'NIP', ['class' => 'form-label']) !!}
                    {!! Form::text('nip', null, ['required', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Tempat Lahir', ['class' => 'form-label']) !!}
                    {!! Form::text('tempat_lahir', null, ['required', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Tanggal Lahir', ['class' => 'form-label']) !!}

                    {!! Form::text('tanggal_lahir', null, ['required', 'class' => 'form-control flatpickr flatpickr-human-friendly', 'placeholder' => 'Silahkan pilih tanggal']) !!}
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Pangkat / Golongan', ['class' => 'form-label']) !!}
                    <select required name="pangkat" class="form-control selectPangkat">
                        <option disabled readonly hidden selected>Pilih pangkat/golongan</option>
                        {{-- Golongan IV --}}
                        <optgroup label="Golongan IV">
                            <option value="Pembina Utama">Pembina Utama / IV.e</option>
                            <option value="Pembina Utama Muda">Pembina Utama Muda / IV.c</option>
                            <option value="Pembina Tingkat I">Pembina Tingkat I / IV.b</option>
                            <option value="Pembina">Pembina / IV.a</option>
                        </optgroup>

                        {{-- Golongan III --}}
                        <optgroup label="Golongan III">
                            <option value="Penata Tingkat I">Penata Tingkat I / III.d</option>
                            <option value="Penata">Penata / III.c</option>
                            <option value="Penata Muda Tingkat I">Penata Muda Tingkat I / III.b</option>
                            <option value="Penata Muda">Penata Muda / III.a</option>
                        </optgroup>

                        {{-- Golongan II --}}
                        <optgroup label="Golongan II">
                            <option value="Pengatur Tingkat I">Pengatur Tingkat I / II.d</option>
                            <option value="Pengatur">Pengatur / II.c</option>
                            <option value="Pengatur Muda Tingkat I">Pengatur Muda Tingkat I / II.b</option>
                            <option value="Pengatur Muda">Pengatur Muda / II.a</option>
                        </optgroup>


                        {{-- Golongan II --}}
                        <optgroup label="Golongan I">
                            <option value="Juru Tingkat I">Juru Tingkat I / I.d</option>
                            <option value="Juru">Juru / I.c</option>
                            <option value="Juru Muda Tingkat I">Juru Muda Tingkat I / I.b</option>
                            <option value="Juru Muda">Juru Muda / I.a</option>
                        </optgroup>

                    </select>
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Masa Kerja Golongan', ['class' => 'form-label']) !!}
                    {!! Form::text('mulai_kerja', null, ['required', 'class' => 'form-control flatpickr flatpickr-human-friendly', 'placeholder' => 'Silahkan pilih tanggal']) !!}
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Status Perkawinan', ['class' => 'form-label']) !!}
                    <select required name="status_kawin" class="form-control">
                        <option disabled readonly hidden selected>Pilih status kawin</option>
                        <option value="bk">Belum Kawin</option>
                        <option value="k">Kawin</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Status Pegawai', ['class' => 'form-label']) !!}
                    <select required name="status_pegawai" class="form-control">
                        <option disabled readonly hidden selected>Pilih status pegawai</option>
                        <option value="asn">Aparatur Sipil Negara - PNS</option>
                        <option value="non">Aparatur Sipil Negara - PPPK</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Unit Kerja', ['class' => 'form-label']) !!}
                    <select required name="unit_kerja" class="form-control">
                        <option disabled readonly hidden selected>Pilih unit kerja</option>
                        <option value="Kecamatan Regol">Kecamatan Regol</option>
                        <option value="Kelurahan Cigereleng Kecamatan Regol">Kelurahan Cigereleng Kecamatan Regol</option>
                        <option value="Kelurahan Ancol Kecamatan Regol">Kelurahan Ancol Kecamatan Regol</option>
                        <option value="Kelurahan Pungkur Kecamatan Regol">Kelurahan Pungkur Kecamatan Regol</option>
                        <option value="Kelurahan Balonggede Kecamatan Regol">Kelurahan Balonggede Kecamatan Regol</option>
                        <option value="Kelurahan Ciseureuh Kecamatan Regol">Kelurahan Ciseureuh Kecamatan Regol</option>
                        <option value="Kelurahan Ciateul Kecamatan Regol">Kelurahan Ciateul Kecamatan Regol</option>
                        <option value="Kelurahan Pasirluyu Kecamatan Regol">Kelurahan Pasirluyu Kecamatan Regol</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    {!! Form::label('', 'Jabatan', ['class' => 'form-label']) !!}
                    {!! Form::text('jabatan', null, ['required', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group mb-3">
                    <button type="submit" class="btn btn-success">
                        Submit Data
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('addJS')
<script src="{{ url('assets/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ url('assets/app/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script>
    $(".flatpickr").flatpickr({
        dateFormat: "d M Y"
    });
    $('.selectPangkat').select2({
        placeholder: 'Pilih pangkat'
    });
</script>
@endsection