@extends('layouts.app')

@section('addCSS')
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default bg-secondary">
        <h3 class="block-title text-white">List Aparatur Sipil Negara - PNS/PPPK</h3>
        <div class="block-options">

            <button data-bs-toggle="modal" data-bs-target="#modal-block-print" class="btn btn-block btn-success">
                <i class="fas fa-print"></i> Print Semua
            </button>
            <a href="{{ route('karyawan.create') }}" class="btn btn-block btn-info">
                <i class="fas fa-plus"></i> Tambah Data
            </a>
        </div>
    </div>
    <div class="block-content block-content-full">
        <table class="table table-bordered table-striped" id="karyawanDataTable">
            <thead>
                <tr>
                    <th class="text-center" style="width: 80px">NIP</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center" style="width: 230px">Pangkat / Gol. Ruang</th>
                    <th class="text-center" style="width: 80px">Status Pegawai</th>
                    <th class="text-center" style="width: 218px">Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<div class="modal" id="modal-block-print" tabindex="-1" role="dialog" aria-labelledby="modal-block-print" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-rounded mb-0">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Print Semua</h3>
                </div>
                {!! Form::open(['method' => 'POST', 'route' => 'print.all']) !!}
                <div class="block-content block-content-full">
                    <div class="mb-3">
                        {!! Form::label('', 'Bulan', ['class' => 'form-label']) !!}
                        {!! Form::select('bulan', [
                        '01' => 'Januari',
                        '02' => 'Februari',
                        '03' => 'Maret',
                        '04' => 'April',
                        '05' => 'Mei',
                        '06' => 'Juni',
                        '07' => 'Juli',
                        '08' => 'Agustus',
                        '09' => 'September',
                        '10' => 'Oktober',
                        '11' => 'Nopember',
                        '12' => 'Desember'
                        ], null, ['class' => 'form-control', 'placeholder' => 'Pilih bulan']) !!}
                    </div>
                    <div class="mb-3">
                        {!! Form::label('', 'Tahun', ['class' => 'form-label']) !!}
                        {!! Form::select('tahun', [
                        '2019' => '2019',
                        '2020' => '2020',
                        '2021' => '2021',
                        '2022' => '2022',
                        '2023' => '2023',
                        '2024' => '2024',
                        '2025' => '2025',
                        '2026' => '2026',
                        '2027' => '2027',
                        '2028' => '2028',
                        '2029' => '2029',
                        '2030' => '2030',
                        '2031' => '2031'
                        ], null, ['class' => 'form-control', 'placeholder' => 'Pilih tahun']) !!}
                    </div>
                </div>
                <div class="block-content block-content-full text-end bg-body">
                    <button type="button" class="btn btn-sm btn-alt-secondary me-1" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-print"></i> Print
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>



<div class="modal" id="modal-block-delete" tabindex="-1" role="dialog" aria-labelledby="modal-block-delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-rounded mb-0">
                <div class="block-header bg-danger">
                    <h3 class="block-title text-white">Hapus Data</h3>
                </div>
                {!! Form::open(['method' => 'POST', 'route' => 'karyawan.remove']) !!}
                <div class="block-content block-content-full">
                    <div class="alert alert-danger d-flex align-items-center justify-content-between" role="alert">
                        <div class="flex-grow-1 me-3">
                            <p class="text-center mb-0">
                                Apakah kamu yakin akan menghapus data ini?
                                {!! Form::hidden('id', null, ['class' => 'delete_id']) !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full text-end bg-body">
                    <button type="button" class="btn btn-sm btn-alt-secondary me-1" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('addJS')
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        const t = $('#karyawanDataTable').DataTable({
            ordering: false,
            searching: true,
            responsive: true,
            language: {
                emptyTable: "No data available",
                sSearchPlaceholder: 'Cari data pegawai...',
                lengthMenu: '_MENU_',
                search: '_INPUT_',
                paginate: {
                    previous: '<i class="fas fa-chevron-left"></i>',
                    next: '<i class="fas fa-chevron-right"></i>'
                }
            },
            columns: [{
                    data: "nip",
                    name: "nip"
                },
                {
                    data: "nama_lengkap",
                    name: "nama_lengkap"
                },
                {
                    data: "pangkat",
                    name: "pangkat"
                },
                {
                    data: "status_pegawai",
                    name: "status_pegawai"
                },
                {
                    data: "action"
                }
            ],
            processing: true,
            serverSide: true,
            ajax: {
                "url": '{{ route("dt.karyawan") }}',
                "type": "POST",
                "data": function(d) {}
            },
            deferRender: true,
            "initComplete": function(settings, json) {
                $('.del-btn').on('click', function() {
                    $('.delete_id').val($(this).attr('data-id'));
                });
            },
            "drawCallback": function(settings) {}
        });

    });
</script>
@endsection