<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Gaji</title>

    <style>
        .--slip-gaji-preview {
            font-family: serif;
            color: black;
            padding: 12px;
            max-width: 1000px;
            margin: auto;
        }

        .text-center {
            text-align: center;
        }

        .--kop {
            padding: 16px 0;
            text-align: center;
        }

        .--kop table {
            margin: auto;
            vertical-align: center;
            border-bottom: 4px solid black;
            padding: 12px 0;
        }

        .--kop h3 {
            font-size: 24.5px;
            margin: 0;
        }

        .--kop h1 {
            margin: 0;
        }

        .--kop p {
            margin: 0;
            font-size: 14px;
        }

        .--header {
            margin: 0;
        }

        .--header h5 {
            margin: 0;
        }

        .--header p {
            margin: 0;
        }

        .--biodata {
            padding: 12px 5%;
            font-size: 14px;
        }

        .--biodata p {
            margin: 0;
        }

        .--penghasilan table,
        .--potongan table {
            border-collapse: collapse;
        }

        .--penghasilan table th,
        .--potongan table th {
            border: 1px solid black;
            background-color: lightgray;
            font-weight: bold;
            text-align: left;
            padding: 2px 6px;
        }

        .--penghasilan td,
        .--potongan td {
            font-size: 14px;
            border: 1px solid black;
            padding: 2px 4px;
        }

        .--penghasilan td:first-child,
        .--potongan td:first-child {
            text-align: center;
        }

        .--penghasilan,
        .--potongan,
        .--footer {
            padding: 12px 5%;
        }

        .denomination {
            display: inline-block;
            margin: 0;
            width: 10%;
        }

        .--penghasilan td span:not(.denomination),
        .--potongan td span:not(.denomination) {
            text-align: right;
            display: inline-block;
            width: 85%;
        }

        p {
            margin: 0;
        }

        @media print {
            .--footer {
                position: fixed;
                bottom: 100px;
                left: 0;
                right: 0;
            }
        }

        .--footer p {
            margin: 0;
        }

        .--ttd {
            width: 35%;
            margin: 16px 0;
            margin-left: auto;
            text-align: right;
        }

        .--note p {
            font-size: 12px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="--slip-gaji-preview block">
        <div class="--kop">
            <table width="90%">
                <tr>
                    <td width="100px">
                        <img src="{{ url('assets/img/kota-bdg.png') }}" width="100px" alt="">
                    </td>
                    <td>
                        <h3>PEMERINTAH KOTA BANDUNG</h3>
                        <h1>KECAMATAN REGOL</h1>
                        <p>Jl. Denki No. 54 Bandung 40255 Telp. (022) 5202489</p>
                    </td>
                </tr>
            </table>
        </div>
        {{-- Stylize Increment --}}
        @php
        $mo = str_pad($h->month_code, 2, '0', STR_PAD_LEFT);
        $inc = str_pad($h->increment, 3, '0', STR_PAD_LEFT);
        @endphp
        <div class="--header">
            <div class="text-center">
                <h5 style="text-decoration: underline;">SURAT KETERANGAN PERINCIAN PENGHASILAN</h5>
                <p>Nomor : KU.07/{{ $inc }}-Kec.Rgl/SKPG/{{ $mo }}/{{$h->year_code}}</p>
            </div>
        </div>

        <div class="--biodata">

            <p align="justify">
                Yang bertandatangan dibawah ini, Bendahara Pengeluaran / Pengelola Gaji Aparatur Sipil Negara pada Pemerintah Kota
                Bandung Kecamatan Regol dengan ini menyatakan bahwa :
            </p>
            <table width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td width="50%">Nama</td>
                                <td>: {{ $h->nama_lengkap }}</td>
                            </tr>
                            <tr>
                                <td>NIP</td>
                                <td>: {{ $h->nip }}</td>
                            </tr>
                            <tr>
                                <td>Tempat / Tgl. Lahir</td>
                                <td>: {{ $h->tempat_lahir.', '.date('d M Y', strtotime($h->tanggal_lahir)) }}</td>
                            </tr>
                            <tr>
                                <td>Pangkat / Gol. Ruang</td>
                                <td>: {{ $h->pangkat.' - '.$h->golongan }}</td>
                            </tr>
                            <tr>
                                <td>Masa Kerja Golongan</td>
                                @php
                                $start = $h->mulai_kerja;
                                $today = date("Y-m-d");
                                $diff = date_diff(date_create($start), date_create($today));
                                @endphp
                                <td>: {{ $diff->format('%y'). ' Tahun '.$diff->format('%M'). ' Bulan' }}</td>
                            </tr>
                        </table>
                    </td>
                    <td style="display: flex; align-items: start;">
                        <table>
                            <tr>
                                <td>Status Perkawinan</td>
                                @if($h->status_kawin == 'k')
                                <td>
                                    : <del>Belum Kawin</del> / Kawin
                                </td>
                                @else
                                <td>
                                    : Belum Kawin / <del>Kawin</del>
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Status Pegawai</td>
                                @if($h->status_pegawai == 'asn')
                                <td>: Aparatur Sipil Negara - PNS</td>
                                @else
                                <td>: Aparatur Sipil Negara - PPPK</td>
                                @endif
                            </tr>
                            <tr>
                                <td>Unit Kerja</td>
                                <td>: {{ $h->unit_kerja }}</td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td>: {{ $h->jabatan }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <div class="--penghasilan">
            <table width="100%">
                <thead>
                    <tr>
                        <th colspan=4>Perincian Penghasilan Bulan : {{ $month }} {{ $h->year_code }}</th>
                    </tr>
                </thead>
                <tr>
                    <td width="5%">1.</td>
                    <td width="45%">Gaji Pokok</td>
                    <td width="25%"><span class="denomination">Rp</span> <span>{{ number_format($h->gaji_pokok) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Tunjangan Suami / Istri</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_pasangan) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Tunjangan Anak</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_anak) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Tunjangan Struktural</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_struktural) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Tunjangan Umum</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_umum) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>Tunjangan Fungsional</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_fungsional) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Tunjangan Khusus</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_khusus) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>Tunjangan Beras</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_beras) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>9.</td>
                    <td>Tunjangan Pajak PPh Ps. 21</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tunjangan_pph) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>10.</td>
                    <td>Pembulatan</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->pembulatan) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right; padding-right: 16px;">
                        <b>Total Gaji</b>
                    </td>
                    <td>
                        <b>
                            <span class="denomination">Rp</span> <span>{{ number_format($h->total_one) }}</span>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td>11.</td>
                    <td width="135px">TPP-ASN</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tpp_asn) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right; padding-right: 16px;">
                        <b>Sub Total Penghasilan (1-11)</b>
                    </td>
                    <td>
                        <b>
                            <span class="denomination">Rp</span> <span>{{ number_format($h->subtotal_one) }}</span>
                        </b>
                    </td>
                </tr>
            </table>
        </div>
        <div class="--potongan">

            <table width="100%">
                <thead>
                    <tr>
                        <th colspan=4>Pemotongan - Pemotongan</th>
                    </tr>
                </thead>
                <tr>
                    <td width="5%">12.</td>
                    <td width="45%">Pemotongan Pajak PPh Ps. 21</td>
                    <td width="25%"><span class="denomination">Rp</span> <span>{{ number_format($h->pemotongan_pph) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>13.</td>
                    <td>Pemotongan Iuran Wajib Pegawai 1%</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->iuran_wajib_one) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>14.</td>
                    <td>Pemotongan Iuran Wajib Pegawai 8%</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->iuran_wajib_eight) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>15.</td>
                    <td>Pemotongan Tabungan Perumahan</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->tabungan_perumahan) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>16.</td>
                    <td>Pemotongan Hutang Bank BJB</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->hutang_bjb) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>17.</td>
                    <td>Pemotongan Hutang KPKB</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->hutang_kpkb) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>18.</td>
                    <td>Pemotongan Hutang BPR</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->hutang_bpr) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td>19.</td>
                    <td>Pemotongan Zakat Profesi</td>
                    <td><span class="denomination">Rp</span> <span>{{ number_format($h->zakat) }}</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right; padding-right: 16px;">
                        <b>Sub Total Pemotongan (12-19)</b>
                    </td>
                    <td>
                        <b>
                            <span class="denomination">Rp</span> <span>{{ number_format($h->subtotal_two) }}</span>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <h4 style="margin: 0;text-align: right; padding-right: 16px;">Jumlah yang diterima</h4>
                    </td>
                    <td>
                        <h4 style="margin: 0;">
                            <span class="denomination">Rp</span> <span>{{ number_format($h->grandtotal) }}</span>
                        </h4>
                    </td>
                </tr>
            </table>
            <p style="margin-top: 12px;">Demikianlah Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</p>
        </div>

        <div class="--footer">
            <div class="--ttd">
                <div>
                    <p>Bandung, {{ date("d F Y", strtotime($h->print_date)) }}</p>
                    <b>Pengelola Gaji</b>
                    <br>
                    <br>
                    <br>
                    <br>
                    <b><u>Rudi Rustandi</u></b>
                    <p>NIP. 19670622 200701 1 013</p>
                </div>
            </div>
            <div class="--note">
                <p>
                    <i>Motto Juang Kecamatan Regol : Gerakan Pembangunan Masyarakat Regol Mandiri ( <b>GERBANG MAREMA</b> )</i>
                </p>
            </div>
        </div>
    </div>
    <script>
        window.onload = window.print();
    </script>
</body>

</html>