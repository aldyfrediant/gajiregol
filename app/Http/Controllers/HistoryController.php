<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Karyawan;
use App\Models\Gaji;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HistoryController extends Controller
{

    public function show($id){
        $history = History::find($id);
        switch ($history->month_code){
            case 1:
                $month = 'Januari';
                break;
            case 2:
                $month = 'Pebruari';
                break;
            case 3:
                $month = 'Maret';
                break;
            case 4:
                $month = 'April';
                break;
            case 5:
                $month = 'Mei';
                break;
            case 6:
                $month = 'Juni';
                break;
            case 7:
                $month = 'Juli';
                break;
            case 8:
                $month = 'Agustus';
                break;
            case 9:
                $month = 'September';
                break;
            case 10:
                $month = 'Oktober';
                break;
            case 11:
                $month = 'Nopember';
                break;
            case 12:
                $month = 'Desember';
                break;
        }
        return view('pages.karyawan.invoice')
            ->with('h', $history)
            ->with('month', $month);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $check = History::where('id_karyawan', '=', $id)
                        ->where('month_code', '=', $request->bulan)
                        ->where('year_code', '=', $request->tahun)
                        ->first();
        if($check != null){
            return redirect()->route('history.show', $check->id);
        }
        $karyawan = Karyawan::find($id);
        $gaji = Gaji::where('id_karyawan', '=', $id)->first();
        $history = new History;

        $history->nama_lengkap = $karyawan->nama_lengkap;
        $history->nip = $karyawan->nip;
        $history->tempat_lahir = $karyawan->tempat_lahir;
        $history->tanggal_lahir = $karyawan->tanggal_lahir;
        $history->pangkat = $karyawan->pangkat;
        $history->golongan = $karyawan->golongan;
        $history->mulai_kerja = $karyawan->mulai_kerja;
        $history->status_kawin = $karyawan->status_kawin;
        $history->status_pegawai = $karyawan->status_pegawai;
        $history->unit_kerja = $karyawan->unit_kerja;
        $history->jabatan = $karyawan->jabatan;
        $history->gaji_pokok = $gaji->gaji_pokok;
        $history->tunjangan_pasangan = $gaji->tunjangan_pasangan;
        $history->tunjangan_anak = $gaji->tunjangan_anak;
        $history->tunjangan_struktural = $gaji->tunjangan_struktural;
        $history->tunjangan_umum = $gaji->tunjangan_umum;
        $history->tunjangan_fungsional = $gaji->tunjangan_fungsional;
        $history->tunjangan_khusus = $gaji->tunjangan_khusus;
        $history->tunjangan_beras = $gaji->tunjangan_beras;
        $history->tunjangan_pph = $gaji->tunjangan_pph;
        $history->pembulatan = $gaji->pembulatan;
        $history->tpp_asn = $gaji->tpp_asn;
        $history->pemotongan_pph = $gaji->pemotongan_pph;
        $history->iuran_wajib_one = $gaji->iuran_wajib_one;
        $history->iuran_wajib_eight = $gaji->iuran_wajib_eight;
        $history->tabungan_perumahan = $gaji->tabungan_perumahan;
        $history->hutang_bjb = $gaji->hutang_bjb;
        $history->hutang_kpkb = $gaji->hutang_kpkb;
        $history->hutang_bpr = $gaji->hutang_bpr;
        $history->zakat = $gaji->zakat;
        $history->total_one = $gaji->total_one;
        $history->subtotal_one = $gaji->subtotal_one;
        $history->subtotal_two = $gaji->subtotal_two;
        $history->grandtotal = $gaji->grandtotal;
        $history->id_karyawan = $id;

        // Generate Month Code
        $history->month_code = $request->bulan;

        // Generate Year Code
        $history->year_code = $request->tahun;
        $history->print_date = Carbon::now();


        // Generate Increment
        $old = History::where('month_code','=', $history->month_code)->orderBy('month_code', 'DESC')->first();
        
        if($old){
            $prev = $old->increment;
            $history->increment = $prev+1;
        } else {
            $history->increment = 1;
        }

        $history->save();
        
        return redirect()->route('history.show', $history->id);
    }

    public function previewAll($month, $year){
        $history = History::where('month_code', '=', $month)
                    ->where('year_code', '=', $year)
                    ->get();

        return view ('pages.karyawan.invoice-all')
            ->with('history', $history)
            ->with('month', $month);
    }

    public function generateMonthly(Request $request){
        $karyawan = Karyawan::all();
        $now = Carbon::now();
        $month = $request->bulan;
        $year = $request->tahun;

        foreach($karyawan as $k){
            $check = History::where('id_karyawan', '=', $k->id)
                        ->where('month_code', '=', $month)
                        ->where('year_code', '=', $year)
                        ->first();
            if($check != null){
                //
            } else {
                $gaji = Gaji::where('id_karyawan', '=', $k->id)->first();
                $history = new History;
    
                $history->nama_lengkap = $k->nama_lengkap;
                $history->nip = $k->nip;
                $history->tempat_lahir = $k->tempat_lahir;
                $history->tanggal_lahir = $k->tanggal_lahir;
                $history->pangkat = $k->pangkat;
                $history->golongan = $k->golongan;
                $history->mulai_kerja = $k->mulai_kerja;
                $history->status_kawin = $k->status_kawin;
                $history->status_pegawai = $k->status_pegawai;
                $history->unit_kerja = $k->unit_kerja;
                $history->jabatan = $k->jabatan;
                $history->gaji_pokok = $gaji->gaji_pokok;
                $history->tunjangan_pasangan = $gaji->tunjangan_pasangan;
                $history->tunjangan_anak = $gaji->tunjangan_anak;
                $history->tunjangan_struktural = $gaji->tunjangan_struktural;
                $history->tunjangan_umum = $gaji->tunjangan_umum;
                $history->tunjangan_fungsional = $gaji->tunjangan_fungsional;
                $history->tunjangan_khusus = $gaji->tunjangan_khusus;
                $history->tunjangan_beras = $gaji->tunjangan_beras;
                $history->tunjangan_pph = $gaji->tunjangan_pph;
                $history->pembulatan = $gaji->pembulatan;
                $history->tpp_asn = $gaji->tpp_asn;
                $history->pemotongan_pph = $gaji->pemotongan_pph;
                $history->iuran_wajib_one = $gaji->iuran_wajib_one;
                $history->iuran_wajib_eight = $gaji->iuran_wajib_eight;
                $history->tabungan_perumahan = $gaji->tabungan_perumahan;
                $history->hutang_bjb = $gaji->hutang_bjb;
                $history->hutang_kpkb = $gaji->hutang_kpkb;
                $history->hutang_bpr = $gaji->hutang_bpr;
                $history->zakat = $gaji->zakat;
                $history->total_one = $gaji->total_one;
                $history->subtotal_one = $gaji->subtotal_one;
                $history->subtotal_two = $gaji->subtotal_two;
                $history->grandtotal = $gaji->grandtotal;
                $history->id_karyawan = $k->id;
    
                // Generate Month Code
                $history->month_code = $month;
    
                // Generate Year Code
                $history->year_code = $year;
                $history->print_date = $now;
    
    
                // Generate Increment
                $old = History::where('month_code','=', $history->month_code)->orderBy('month_code', 'DESC')->first();
                
                if($old){
                    $prev = $old->increment;
                    $history->increment = $prev+1;
                } else {
                    $history->increment = 1;
                }
    
                $history->save();
            }

            return redirect()->route('print.all.index', [$month, $year]);
        }
    }
}