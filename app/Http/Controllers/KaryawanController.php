<?php

namespace App\Http\Controllers;

use App\Models\Karyawan;
use App\Models\History;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use App\Models\Gaji;
use DB;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.karyawan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.karyawan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kry = new Karyawan;

        $kry->nama_lengkap = $request->nama;
        $kry->nip = $request->nip;
        $kry->tempat_lahir = $request->tempat_lahir;
        $kry->tanggal_lahir = date("Y-m-d", strtotime($request->tanggal_lahir));
        $kry->mulai_kerja = date("Y-m-d", strtotime($request->mulai_kerja));
        $kry->pangkat = $request->pangkat;
        switch ($kry->pangkat) {
                // Gol IV
            case 'Pembina Utama':
                $kry->golongan = "IV.e";
                break;
            case 'Pembina Utama Muda':
                $kry->golongan = "IV.c";
                break;
            case 'Pembina Tingkat I':
                $kry->golongan = "IV.b";
                break;
            case 'Pembina':
                $kry->golongan = "IV.a";
                break;

                // Gol III
            case 'Penata Tingkat I':
                $kry->golongan = "III.d";
                break;
            case 'Penata':
                $kry->golongan = "III.c";
                break;
            case 'Penata Muda Tingkat I':
                $kry->golongan = "III.b";
                break;
            case 'Penata Muda':
                $kry->golongan = "III.a";
                break;

                // Gol II
            case 'Pengatur Tingkat I':
                $kry->golongan = "II.d";
                break;
            case 'Pengatur':
                $kry->golongan = "II.c";
                break;
            case 'Pengatur Muda Tingkat I':
                $kry->golongan = "II.b";
                break;
            case 'Pengatur Muda':
                $kry->golongan = "II.a";
                break;

                // Gol I
            case 'Juru Tingkat I':
                $kry->golongan = "I.d";
                break;
            case 'Juru':
                $kry->golongan = "I.c";
                break;
            case 'Juru Muda Tingkat I':
                $kry->golongan = "I.b";
                break;
            case 'Juru Muda':
                $kry->golongan = "I.a";
                break;
        }
        $kry->status_kawin = $request->status_kawin;
        $kry->status_pegawai = $request->status_pegawai;
        $kry->unit_kerja = $request->unit_kerja;
        $kry->jabatan = $request->jabatan;
        $kry->save();

        return redirect()->route('karyawan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function show(Karyawan $karyawan)
    {
        $gaji = Gaji::where('id_karyawan', '=', $karyawan->id)->first();
        if ($gaji != null) {
            $gaji = $gaji;
            $route = 'update';
        } else {
            $gaji = new Gaji;
            $route = 'create';
        }
        $history = History::where('id_karyawan', '=', $karyawan->id)
            ->orderBy('year_code', 'DESC')
            ->orderBy('month_code', 'DESC')->get();

        return view('pages.karyawan.update')
            ->with('k', $karyawan)
            ->with('route', $route)
            ->with('history', $history)
            ->with('g', $gaji);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Karyawan $karyawan)
    {
        $kry = $karyawan;

        $kry->nama_lengkap = $request->nama;
        $kry->nip = $request->nip;
        $kry->tempat_lahir = $request->tempat_lahir;
        $kry->tanggal_lahir = date("Y-m-d", strtotime($request->tanggal_lahir));
        $kry->mulai_kerja = date("Y-m-d", strtotime($request->mulai_kerja));
        $kry->pangkat = $request->pangkat;

        switch ($kry->pangkat) {
                // Gol IV
            case 'Pembina Utama':
                $kry->golongan = "IV.e";
                break;
            case 'Pembina Utama Muda':
                $kry->golongan = "IV.c";
                break;
            case 'Pembina Tingkat I':
                $kry->golongan = "IV.b";
                break;
            case 'Pembina':
                $kry->golongan = "IV.a";
                break;

                // Gol III
            case 'Penata Tingkat I':
                $kry->golongan = "III.d";
                break;
            case 'Penata':
                $kry->golongan = "III.c";
                break;
            case 'Penata Muda Tingkat I':
                $kry->golongan = "III.b";
                break;
            case 'Penata Muda':
                $kry->golongan = "III.a";
                break;

                // Gol II
            case 'Pengatur Tingkat I':
                $kry->golongan = "II.d";
                break;
            case 'Pengatur':
                $kry->golongan = "II.c";
                break;
            case 'Pengatur Muda Tingkat I':
                $kry->golongan = "II.b";
                break;
            case 'Pengatur Muda':
                $kry->golongan = "II.a";
                break;

                // Gol I
            case 'Juru Tingkat I':
                $kry->golongan = "I.d";
                break;
            case 'Juru':
                $kry->golongan = "I.c";
                break;
            case 'Juru Muda Tingkat I':
                $kry->golongan = "I.b";
                break;
            case 'Juru Muda':
                $kry->golongan = "I.a";
                break;
        }
        $kry->status_kawin = $request->status_kawin;
        $kry->status_pegawai = $request->status_pegawai;
        $kry->unit_kerja = $request->unit_kerja;
        $kry->jabatan = $request->jabatan;
        $kry->save();

        return redirect()->route('karyawan.show', $kry->id);
    }


    public function getDatatableKaryawans(Request $request)
    {
        $kry = DB::table('karyawans')
            ->orderBy('nama_lengkap', 'ASC');

        return DataTables::of($kry)
            ->addColumn('action', function ($k) {
                return ('
                <div class="text-center">
                    <a href="' . route('karyawan.show', $k->id) . '" class="btn btn-sm btn-warning">
                        <i class="fas fa-edit"></i> Edit
                    </a>
                    <a href="' . route('gaji.print', [$k->id]) . '" class="btn btn-sm btn-success">
                        <i class="fas fa-print"></i> Print
                    </a>
                    <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#modal-block-delete"  class="btn btn-sm btn-danger del-btn" data-id="' . $k->id . '">
                        <i class="fas fa-trash"></i> Hapus
                    </a>
                </div>
                ');
            })
            ->editColumn('status_pegawai', function ($k) {
                if ($k->status_pegawai == 'asn') {
                    return ('<span class="badge rounded-pill bg-info">ASN - PNS</span>');
                } else {
                    return ('<span class="badge rounded-pill bg-dark">ASN - PPPK</span>');
                }
            })
            ->editColumn('pangkat', function ($k) {
                return ($k->pangkat . ' / ' . $k->golongan);
            })
            ->rawColumns(['action', 'status_pegawai'])
            ->make(true);
    }

    public function remove(Request $request)
    {
        $kry = Karyawan::find($request->id);
        $kry->delete();
        $gaji = Gaji::where('id_karyawan', '=', $kry->id)->first();
        if ($gaji != null) {
            $gaji->delete();
        }

        return redirect()->route('karyawan.index');
    }
}
