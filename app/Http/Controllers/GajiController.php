<?php

namespace App\Http\Controllers;

use App\Models\Gaji;
use App\Models\Karyawan;
use Illuminate\Http\Request;
use Carbon\Carbon;

class GajiController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gaji = New Gaji;

        $gaji->gaji_pokok = str_replace(',', '', $request->gaji_pokok);
        $gaji->tunjangan_pasangan = str_replace(',', '', $request->tunjangan_pasangan);
        $gaji->tunjangan_anak = str_replace(',', '', $request->tunjangan_anak);
        $gaji->tunjangan_struktural = str_replace(',', '', $request->tunjangan_struktural);
        $gaji->tunjangan_umum = str_replace(',', '', $request->tunjangan_umum);
        $gaji->tunjangan_fungsional = str_replace(',', '', $request->tunjangan_fungsional);
        $gaji->tunjangan_khusus = str_replace(',', '', $request->tunjangan_khusus);
        $gaji->tunjangan_beras = str_replace(',', '', $request->tunjangan_beras);
        $gaji->tunjangan_pph = str_replace(',', '', $request->tunjangan_pph);
        $gaji->pembulatan = str_replace(',', '', $request->pembulatan);
        $gaji->total_one = str_replace(',', '', $request->total_one);
        $gaji->tpp_asn = str_replace(',', '', $request->tpp_asn);
        $gaji->subtotal_one = str_replace(',', '', $request->subtotal_one);
        $gaji->pemotongan_pph = str_replace(',', '', $request->pemotongan_pph);
        $gaji->iuran_wajib_one = str_replace(',', '', $request->iuran_wajib_one);
        $gaji->iuran_wajib_eight = str_replace(',', '', $request->iuran_wajib_eight);
        $gaji->tabungan_perumahan = str_replace(',', '', $request->tabungan_perumahan);
        $gaji->hutang_bjb = str_replace(',', '', $request->hutang_bjb);
        $gaji->hutang_kpkb = str_replace(',', '', $request->hutang_kpkb);
        $gaji->hutang_bpr = str_replace(',', '', $request->hutang_bpr);
        $gaji->zakat = str_replace(',', '', $request->zakat);
        $gaji->subtotal_two = str_replace(',', '', $request->subtotal_two);
        $gaji->grandtotal = str_replace(',', '', $request->grandtotal);
        $gaji->id_karyawan = $request->id_karyawan;
        $gaji->save();

        return redirect()->route('karyawan.show', $gaji->id_karyawan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gaji  $gaji
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $gaji)
    {
        $gaji = Gaji::find($gaji);

        $gaji->gaji_pokok = str_replace(',', '', $request->gaji_pokok);
        $gaji->tunjangan_pasangan = str_replace(',', '', $request->tunjangan_pasangan);
        $gaji->tunjangan_anak = str_replace(',', '', $request->tunjangan_anak);
        $gaji->tunjangan_struktural = str_replace(',', '', $request->tunjangan_struktural);
        $gaji->tunjangan_umum = str_replace(',', '', $request->tunjangan_umum);
        $gaji->tunjangan_fungsional = str_replace(',', '', $request->tunjangan_fungsional);
        $gaji->tunjangan_khusus = str_replace(',', '', $request->tunjangan_khusus);
        $gaji->tunjangan_beras = str_replace(',', '', $request->tunjangan_beras);
        $gaji->tunjangan_pph = str_replace(',', '', $request->tunjangan_pph);
        $gaji->pembulatan = str_replace(',', '', $request->pembulatan);
        $gaji->total_one = str_replace(',', '', $request->total_one);
        $gaji->tpp_asn = str_replace(',', '', $request->tpp_asn);
        $gaji->subtotal_one = str_replace(',', '', $request->subtotal_one);
        $gaji->pemotongan_pph = str_replace(',', '', $request->pemotongan_pph);
        $gaji->iuran_wajib_one = str_replace(',', '', $request->iuran_wajib_one);
        $gaji->iuran_wajib_eight = str_replace(',', '', $request->iuran_wajib_eight);
        $gaji->tabungan_perumahan = str_replace(',', '', $request->tabungan_perumahan);
        $gaji->hutang_bjb = str_replace(',', '', $request->hutang_bjb);
        $gaji->hutang_kpkb = str_replace(',', '', $request->hutang_kpkb);
        $gaji->hutang_bpr = str_replace(',', '', $request->hutang_bpr);
        $gaji->zakat = str_replace(',', '', $request->zakat);
        $gaji->subtotal_two = str_replace(',', '', $request->subtotal_two);
        $gaji->grandtotal = str_replace(',', '', $request->grandtotal);
        $gaji->id_karyawan = $request->id_karyawan;
        $gaji->save();

        return redirect()->route('karyawan.show', $gaji->id_karyawan);
    }

    public function print($karyawan)
    {
        $karyawan = Karyawan::find($karyawan);
        $gaji = Gaji::where('id_karyawan', '=', $karyawan->id)->first();
        if($gaji == null){
            return redirect()->route('karyawan.show', $karyawan->id);
        }
        return view('pages.karyawan.preview')
            ->with('k', $karyawan)
            ->with('g', $gaji);
    }
    public function printDetail($karyawan, Request $request)
    {
        $karyawan = Karyawan::find($karyawan);
        $gaji = Gaji::where('id_karyawan', '=', $karyawan->id)->first();
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $date = Carbon::now();

        return view('pages.karyawan.invoice')
            ->with('k', $karyawan)
            ->with('g', $gaji)
            ->with('mo', $bulan)
            ->with('ye', $tahun)
            ->with('date', $date);
    }
}
