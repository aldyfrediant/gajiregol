<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\GajiController;
use App\Http\Controllers\HistoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/karyawan');
});

Route::resource('/karyawan', KaryawanController::class);
Route::post('karyawan/delete', [KaryawanController::class, 'remove'])->name('karyawan.remove');
Route::post('karyawan/gaji', [GajiController::class, 'store'])->name('gaji.store');
Route::put('karyawan/gaji/{id}', [GajiController::class, 'update'])->name('gaji.update');
Route::get('karyawan/gaji/{id}/print', [GajiController::class, 'print'])->name('gaji.print');

Route::post('print/gaji/{id}', [HistoryController::class, 'store'])->name('history.store');
Route::get('view/gaji/{id}', [HistoryController::class, 'show'])->name('history.show');

Route::get('print/all/{month}/{year}', [HistoryController::class, 'previewAll'])->name('print.all.index');
Route::post('print/all', [HistoryController::class, 'generateMonthly'])->name('print.all');