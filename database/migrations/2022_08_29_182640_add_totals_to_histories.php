<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('histories', function (Blueprint $table) {
            $table->integer('total_one');
            $table->integer('subtotal_one');
            $table->integer('subtotal_two');
            $table->integer('grandtotal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('histories', function (Blueprint $table) {
            $table->dropColumn('total_one');
            $table->dropColumn('subtotal_one');
            $table->dropColumn('subtotal_two');
            $table->dropColumn('grandtotal');
        });
    }
};
