<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap');
            $table->string('nip');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('pangkat');
            $table->string('golongan');
            $table->date('mulai_kerja');
            $table->string('status_kawin');
            $table->string('status_pegawai');
            $table->string('unit_kerja');
            $table->string('jabatan');
            $table->integer('gaji_pokok');
            $table->integer('tunjangan_pasangan');
            $table->integer('tunjangan_anak');
            $table->integer('tunjangan_struktural');
            $table->integer('tunjangan_umum');
            $table->integer('tunjangan_fungsional');
            $table->integer('tunjangan_khusus');
            $table->integer('tunjangan_beras');
            $table->integer('tunjangan_pph');
            $table->integer('pembulatan');
            $table->integer('tpp_asn');
            $table->integer('pemotongan_pph');
            $table->integer('iuran_wajib_one');
            $table->integer('iuran_wajib_eight');
            $table->integer('tabungan_perumahan');
            $table->integer('hutang_bjb');
            $table->integer('hutang_kpkb');
            $table->integer('hutang_bpr');
            $table->integer('zakat');
            $table->date('print_date');
            $table->unsignedInteger('id_karyawan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
};
